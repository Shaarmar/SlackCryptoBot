import subprocess
from urllib import quote
import re
import json
import requests
import time
import sys


def reboot(): 
    
    return subprocess.call(['./restartSlask.sh']) 

def on_message(msg, server):
    text = msg.get("text", "")
    split_text = text.split(" ")
    text = " ".join(split_text)
    keyword = "!reboot"
    
    
    if text.find(keyword) == -1:
        pass
    else:
       return reboot()     
