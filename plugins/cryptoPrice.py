
import coinmarketcap
from urllib import quote
import re
import json
import requests
import time
import sys


##reload(sys)
##sys.setdefaultencoding('utf-8')
##
##def price(searchterm):
##    #url = "http://coinmarketcap.northpole.ro/api/v6/{0}.json"
##    url = "https://coinmarketcap.northpole.ro/ticker.json?symbol={0}&page=0&size=20"
##    url2 = "http://coincap.io/page/{0}"
##    url3 = "https://api.coinmarketcap.com/v1/ticker/{0}/"
##
##    url = url.format(searchterm)
##    url2 = url2.format(searchterm)
##    url3 = url3.format(searchterm)
##
##    print(url)
##    print(url2)
##    print(url3)
##
##    try:
##
##        #dat = requests.get(url,timeout = 5).json()
##        dat = requests.get(url3, timeout = 2).json()
##        #dat = requests.get(url).json()
##
##        #print(dat['markets'][0]['marketCap']['usd'])
##        #print(dat['markets'][0]['position'])
##        #print(dat['markets'][0]['price']['usd'])
##        #print(dat['markets'][0]['name']).replace(" ", "")
##        
##
##
##        #print(dat)
##        #dat['markets'][0]['marketCap']['usd'] = ("{:,.2f}".format(dat['markets'][0]['marketCap']['usd']))
##        #dat['markets'][0]['volume24']['usd'] = ("{:,.2f}".format(dat['markets'][0]['volume24']['usd']))
##        #dat['markets'][0]['price']['usd'] = ("{:,.2f}".format(dat['markets'][0]['price']['usd']))
##        
##        #dat['market_cap_usd'] = ("{:,.2f}".format(dat['market_cap_usd']))
##        #dat['24h_volume_usd'] = ("{:,.2f}".format(dat['24h_volume_usd']))
##        #dat['price_usd'] = ("{:,.2f}".format(dat['price_usd']))
##        
##        dat[0]['market_cap_usd'] = ("{:,.2f}".format(float(dat[0]['market_cap_usd'])))
##        dat[0]['24h_volume_usd'] = ("{:,.2f}".format(float(dat[0]['24h_volume_usd'])))
##        dat[0]['price_usd'] = ("{:,.2f}".format(float(dat[0]['price_usd'])))
##
##
##        
##    except (requests.exceptions.Timeout, requests.exceptions.ConnectionError) as err:
##
##        dat2 = requests.get(url2).json()
##        dat2['market_cap'] = ("{:,.2f}".format(dat2['market_cap']))
##        dat2['volume'] = ("{:,.2f}".format(dat2['volume']))
##        dat2['price'] = ("{:,.2f}".format(dat2['price']))
##           
##    
##    # print(dat)
##
##
##        try:
##            #result = '```Coin: ' + dat['name']
##            result = 'Coin: ' + dat[0]['name'] + ', Position: '+str(dat[0]['rank']) + ', Symbol: '+dat[0]['symbol']+', Price: $'+dat[0]['price_usd']+', Market Cap: $'+dat[0]['market_cap_usd']+', Percent 1hr: '+str(dat[0]['percent_change_1h'])+'%, Percent 24hr: '+str(dat[0]['percent_change_24h'])+'%, Percent 7d: '+str(dat[0]['percent_change_7d'])+'%, USD Volume: $'+str(dat[0]['24h_volume_usd'])
##            #+ ', Position: '+str(dat['rank']) + ', Symbol: '+dat['symbol']+', Price: $'+dat['price_usd']+', Market Cap: $'+dat['market_cap_usd']+', Percent 1hr: '+str(dat['percent_change_1h'])+'%, Percent 24hr: '+str(dat['percent_change_24h'])+'%, Percent 7d: '+str(dat['percent_change_7d'])+'%, USD Volume: $'+str(dat['24h_volume_usd'])) + '```'
##            return result
##        except Exception as e:
##            print("is coinmarketcap.com down?")
##
##
##    ''' try:
##        result = '```Coin: ' + dat['markets'][0]['name'].replace(" ", "") + ', Position: '+str(dat['markets'][0]['position']) + ', Symbol: '+dat['markets'][0]['symbol']+', Price: $'+dat['markets'][0]['price']['usd']+', Market Cap: $'+dat['markets'][0]['marketCap']['usd']+', Percent 1hr: '+str(dat['markets'][0]['change1h'])+'%, Percent 24hr: '+str(dat['markets'][0]['change24h'])+'%, Percent 7d: '+str(dat['markets'][0]['change7d'])+'%, USD Volume: $'+str(dat['markets'][0]['volume24']['usd'])+'```'.rstrip('\n')
##        return result
##        #return 'hi'
##    except Exception as e:
##        result = '```Coin: '+dat2['display_name']+', Symbol: '+dat2['id']+', Price: $'+str(dat2['price'])+', Market Cap: $'+str(dat2['market_cap'])+', Volume: '+str(dat2['volume'])+', BTC Price: '+str(dat2['price_btc'])+'btc```'
##        return result '''
##


def price(searchterm):
    url3 = "https://api.coinmarketcap.com/v1/ticker/{0}/"
    data = json.load(open('/home/pi/Desktop/CryptoBot/crypto.json'))
    g = data.get(searchterm)

    url3 = url3.format(g)
    print(url3)
    dat = requests.get(url3).json()
    dat[0]['market_cap_usd'] = ("{:,.2f}".format(float(dat[0]['market_cap_usd'])))
    dat[0]['24h_volume_usd'] = ("{:,.2f}".format(float(dat[0]['24h_volume_usd'])))
    dat[0]['price_usd'] = ("{:,.2f}".format(float(dat[0]['price_usd'])))
    
    if dat[0]['market_cap_usd'] == None:
        dat[0]['market_cap_usd'] = float("0.00")

    
    try:
        result = '```Coin: ' + dat[0]['name'] + ', Position: '+str(dat[0]['rank']) + ', Symbol: '+dat[0]['symbol']+', Price: $'+dat[0]['price_usd']+', Market Cap: $'+dat[0]['market_cap_usd']+'\n'+'Percent 1hr: '+str(dat[0]['percent_change_1h'])+'%, Percent 24hr: '+str(dat[0]['percent_change_24h'])+'%, Percent 7d: '+str(dat[0]['percent_change_7d'])+'%, USD Volume: $'+str(dat[0]['24h_volume_usd'])+'```'
        return result
    except Exception as e:
        print(e)
        
        
def on_message(msg, server):
    text = msg.get("text", "")
    split_text = text.split(" ")
    text = " ".join(split_text)
    keyword = "!price "
    
    
    if text.find(keyword) == -1:
        pass
    else:
        match = re.findall(r"!price (.*)", text)
        if not match: return
        searchterm = match[0].upper()

        try:
            return price(searchterm.upper())
        except Exception as e:
            print(e)
            return 'Please enter a valid symbol :)'
      






