import subprocess
from urllib import quote
import re
import json
import requests
import time
import sys


def gitpull(): 
    subprocess.check_output(['git','pull'])
    return "Updated json file"


def on_message(msg, server):
    text = msg.get("text", "")
    split_text = text.split(" ")
    text = " ".join(split_text)
    keyword = "!pull"
    
    
    if text.find(keyword) == -1:
        pass
    else:
       return gitpull()     

