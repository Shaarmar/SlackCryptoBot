from urllib import quote
import re
import json
import requests
import time
import sys
from prettytable import PrettyTable

j = PrettyTable()


def topTen(searchterm):
    mystring = ''
    print(mystring)
    url = "https://api.coinmarketcap.com/v1/ticker/?limit={0}"
    
    
    if not searchterm.isnumeric():
        return "Please use numbers 1-100 :)"
    try:
        if not 1<= int(searchterm) <= 100:
            return "Please use numbers 1-100 :)"
    except Exception as e:
        return "Please use numbers 1-100 :)"

    url = url.format(searchterm)
    dat = requests.get(url).json()
 
    j.field_names = ["Rank", "Name", "Price", "Market Cap", "Volume", '24hr', '7 Day']
    j.clear_rows()

 
    for x in range(len(dat)):
        dat[x]['market_cap_usd'] = ("{:,.2f}".format(float(dat[x]['market_cap_usd'])))
        dat[x]['24h_volume_usd'] = ("{:,.2f}".format(float(dat[x]['24h_volume_usd'])))
        dat[x]['price_usd'] = ("{:,.2f}".format(float(dat[x]['price_usd']))) 
   
    try:
        for y in range(len(dat)):
            j.add_row([dat[y]['rank'],dat[y]['name'],dat[y]['price_usd'],dat[y]['market_cap_usd'],dat[y]['24h_volume_usd'],dat[y]['percent_change_24h'],dat[y]['percent_change_7d']])
            j.align['Name']= "l"
            j.align['Rank'] = 'c'
            j.align['Price'] = 'r'
            j.align['Market Cap'] = 'r'
            j.align['Volume'] = 'r'
            j.align['24hr'] ='r'
            j.align['7 Day'] ='r'
            
 
        mystring = '```'+j.get_string()+'```'

        return mystring
    except Exception as e:
        print(e)

        
def on_message(msg, server):
    text = msg.get("text", "")
    split_text = text.split(" ")
    text = " ".join(split_text)
    keyword = "!top "
  
    if text.find(keyword) == -1:
        pass
    else:
        match = re.findall(r"!top (.*)", text)
        if not match: return
    
        searchterm = match[0]
   
        return topTen(searchterm)
       
  
