# Slask
### A [Slack](https://slack.com/) chatbot

![](https://travis-ci.org/llimllib/slask.svg)

## Installation

1. Clone the repo
2. `pip install -r requirements.txt`
3. [Create a bot user](https://my.slack.com/services/new/bot) if you don't have one yet, and copy the API Token
4. Copy `config.py.sample` to `config.py` and paste in the token you got in step 4
5. `python slask.py`
6. Invite Slask into any channels you want it in, or just message it in #general. Try typing `!gif dubstep cat` to test it out

![kitten mittens](http://i.imgur.com/xhmD6QO.png)

## Commands

It's super easy to add your own commands! Just create a python file in the plugins directory with an `on_message` function that returns a string.

You can use the `!help` command to print out all available commands and a brief help message about them. `!help <plugin>` will return just the help for a particular plugin.


## Contributors

* [@fsalum](https://github.com/fsalum)
* [@rodvodka](https://github.com/rodvodka)
* [@mattfora](https://github.com/mattfora)
* [@dguido](https://github.com/dguido)
* [@JoeGermuska](https://github.com/JoeGermuska)
